import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Artists from "./containers/Artist/Artist";
import Register from "./containers/Register/Register";
import Layout from "./containers/Layout";
import Albums from "./containers/Album/Album";
import Tracks from "./containers/Track/Track";
import TrackHistory from "./containers/TrakHistory/TrackHistory";
import AddFormAlbum from "./containers/Forms/AddFormAlbum";
import AddFormArtist from "./containers/Forms/AddFormArtist";


class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists} />
                <Route path="/register" exact component={Register} />
                <Route path="/albums/" component={Albums} />
                <Route path="/tracks/" component={Tracks} />
                <Route path="/track_history/" component={TrackHistory} />
                <Route path="/add_Album/" component={AddFormAlbum} />
                <Route path="/add_Artist/" component={AddFormArtist} />
            </Switch>
        </Layout>
    );
  }
}

export default App;