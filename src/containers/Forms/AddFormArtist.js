import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from "@material-ui/core/es/TextField/TextField";
import Button from "@material-ui/core/es/Button/Button";
import AddButton from '@material-ui/icons/CloudUpload';
import Typography from "@material-ui/core/es/Typography/Typography";
import {connect} from "react-redux";
import {fetchLogoutUser} from "../../store/actions/users";
import {push} from "react-router-redux";

const styles = theme => ({
    root: {
        margin: '50px',
        display: 'flex',
        flexDirection: "column",
        flexWrap: 'wrap',
    },
    rightIcon: {
        marginLeft: '20px',
    },
    iconSmall: {
        fontSize: 20,
    },
    formControl: {
        margin: theme.spacing.unit,
        width: 120,
    },
    button: {
        margin: theme.spacing.unit,
        width: '200px'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class AddFormArtist extends React.Component {
    state = {
        age: '',
        name: 'hai',
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { classes } = this.props;

        return (
            <form className={classes.root} autoComplete="off">
                <div>
                    <Typography color='error' variant='display3' >Add artist</Typography>
                </div>
                <TextField
                    required
                    id="name"
                    label="Artist name"
                    className={classes.textField}
                    margin="normal"

                />
                <TextField
                    id="file"
                    type="file"
                    className={classes.textField}
                    helperText="add image album"
                    margin="normal"
                />
                <TextField
                    id="info"
                    label="Label"
                    placeholder="Description album"
                    helperText="enter some description!"
                    fullWidth
                    margin="normal"
                />
                <Button type='submit' className={classes.button} variant="raised" color="secondary">
                    Add Album
                    <AddButton className={classes.rightIcon} />
                </Button>
            </form>
        );
    }
}

AddFormArtist.propTypes = {
    classes: PropTypes.object.isRequired,
};
const mapStateToProps = state => ({
    albums: state.albums.albums
});

const mapDispathToProps = dispatch => ({
    logout: () => dispatch(fetchLogoutUser()),
    route: path => dispatch(push(path))
});

export default connect(mapStateToProps, mapDispathToProps)(withStyles(styles)(AddFormArtist));
