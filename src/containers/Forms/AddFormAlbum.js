import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from "@material-ui/core/es/TextField/TextField";
import Button from "@material-ui/core/es/Button/Button";
import AddButton from '@material-ui/icons/CloudUpload';
import Typography from "@material-ui/core/es/Typography/Typography";
import purple from "@material-ui/core/es/colors/purple";
import {connect} from "react-redux";
import {fetchLogoutUser} from "../../store/actions/users";
import {push} from "react-router-redux";

const styles = theme => ({
    root: {
        margin: '50px',
        display: 'flex',
        flexDirection: "column",
        flexWrap: 'wrap',
    },
    rightIcon: {
        marginLeft: '20px',
    },
    iconSmall: {
        fontSize: 20,
    },
    formControl: {
        margin: theme.spacing.unit,
        width: 120,
    },
    button: {
        margin: theme.spacing.unit,
        width: '200px'
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    cssRoot: {
        color: theme.palette.getContrastText(purple[500]),
        backgroundColor: purple[500],
        '&:hover': {
            backgroundColor: purple[700],
        }
    }
});

class AddFormAlbum extends React.Component {
    state = {
        age: '',
        name: 'hai',
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { classes } = this.props;

        return (
            <form className={classes.root} autoComplete="off">
                <div>
                    <Typography color='error' variant='display3' >Add album form</Typography>
                </div>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="age-helper">Age</InputLabel>
                    <Select
                        required
                        value={this.state.age}
                        onChange={this.handleChange}
                        input={<Input name="age" id="age-helper" />}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                    <FormHelperText>Select Artist</FormHelperText>
                </FormControl>
                <TextField
                    required
                    id="name"
                    label="Album name"
                    className={classes.textField}
                    margin="normal"

                />
                <TextField
                    id="date"
                    type="date"
                    className={classes.textField}
                    helperText="enter date album's!"
                    margin="normal"
                />
                <TextField
                    id="file"
                    type="file"
                    className={classes.textField}
                    helperText="add image album"
                    margin="normal"
                />
                <TextField
                    id="info"
                    label="Label"
                    placeholder="Description album"
                    helperText="enter some description!"
                    fullWidth
                    margin="normal"
                />
                <Button type='submit' className={classes.button} variant="raised" color="secondary">
                    Add Album
                    <AddButton className={classes.rightIcon} />
                </Button>
            </form>
        );
    }
}

AddFormAlbum.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    albums: state.albums.albums
});

const mapDispathToProps = dispatch => ({
    logout: () => dispatch(fetchLogoutUser()),
    route: path => dispatch(push(path))
});

export default connect(mapStateToProps, mapDispathToProps)(withStyles(styles)(AddFormAlbum));
