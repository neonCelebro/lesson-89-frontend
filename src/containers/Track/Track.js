import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import TrackList from "../../components/UI/TrackList/TrackList";
import {addTrackInHistory, fetchTracks} from "../../store/actions/tracks";

class Albums extends Component {
    componentDidMount() {
        this.props.onFetchTracks(this.props.location.pathname);
    }

    render() {
        return (
            <Fragment>
                <PageHeader style={{borderColor: '#6dac8a'}}>
                    Music FM / Artists / Albums / Tracks
                </PageHeader>
                {this.props.tracks.map((track, id) => {
                    return (
                        <TrackList
                            key={id}
                            name={track.name}
                            title={this.props.tracks[0]? this.props.tracks[0].album.name: 'title'}
                            trackHendler={this.props.addTrackInHistory}
                            time={track.time}
                        />
                    )
                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchTracks: idAlbum => dispatch(fetchTracks(idAlbum)),
        addTrackInHistory : idTrack => dispatch(addTrackInHistory(idTrack))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);