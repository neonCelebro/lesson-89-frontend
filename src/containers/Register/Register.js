import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Alert, Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, PageHeader} from "react-bootstrap";
import {loginUser, registerUser} from "../../store/actions/users";
import Button from "@material-ui/core/es/Button/Button";
import RegisterIcon from '@material-ui/icons/Add';
import LoginIcon from '@material-ui/icons/AssignmentInd';




class Register extends Component {
    state = {
        username: '',
        password: '',
        role: "user"

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    registerButtonHandler = event => {
        event.preventDefault();
        this.props.registerUser(this.state);
    };

    loginButtonHandler = event => {
        event.preventDefault();
        this.props.loginUser(this.state);
    };

    fieldHasError = fieldName => {
        return this.props.registerError && this.props.registerError.errors[fieldName];
    };

    render() {
        return (
            <Fragment>
                    <PageHeader style={{borderColor: '#6dac8a'}}>Register new user</PageHeader>
                    <Form horizontal onSubmit={this.submitFormHandler}>

                        <FormGroup
                            controlId="username"
                            validationState={this.fieldHasError('username') ? 'error' : null}
                        >
                            {this.props.loginError &&
                            <Alert bsStyle="danger">{this.props.loginError.message}</Alert>
                            }
                            <Col componentClass={ControlLabel} sm={2}>
                                Username
                            </Col>
                            <Col sm={4}>
                                <FormControl
                                    type="text"
                                    placeholder="Enter username"
                                    name="username"
                                    value={this.state.username}
                                    onChange={this.inputChangeHandler}
                                    autoComplete="off"
                                />
                                {this.fieldHasError('username') &&
                                <HelpBlock>{this.props.registerError.errors.username.message}</HelpBlock>
                                }
                            </Col>
                        </FormGroup>

                        <FormGroup
                            controlId="password"
                            validationState={this.fieldHasError('password') ? 'error' : null}
                        >
                            {this.props.error &&
                            <Alert bsStyle="danger">{this.props.loginError.message}</Alert>
                            }
                            <Col componentClass={ControlLabel} sm={2}>
                                Password
                            </Col>
                            <Col sm={4}>
                                <FormControl
                                    type="password"
                                    placeholder="Enter password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.inputChangeHandler}
                                    autoComplete="new-username"
                                />
                                {this.fieldHasError('password') &&
                                <HelpBlock>{this.props.registerError.errors.password.message}</HelpBlock>
                                }
                            </Col>
                        </FormGroup>

                        <FormGroup>
                            <Col smOffset={2} sm={3} style={{width: '20%'}}>
                                <Button
                                    onClick={this.registerButtonHandler}
                                    variant="raised"
                                    >
                                    Create account
                                    <RegisterIcon style={{marginLeft: '5px'}}/>
                                </Button>
                            </Col>
                            <Col smOffset={0} sm={3}>
                                <Button
                                    onClick={this.loginButtonHandler}
                                    variant="raised"
                                    >
                                    Log In
                                    <LoginIcon style={{marginLeft: '5px'}}/>
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
            </Fragment>

        )
    }
}

const mapStateToProps = state => ({
    registerError: state.users.registerError,
    loginError: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData)),
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);