import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ListItem from '../../components/UI/ListItem/ListItem';
import {fetchAlbums} from "../../store/actions/albums";
import {push} from "react-router-redux";

class Albums extends Component {
    componentDidMount() {
        this.props.onFetchAlbums(this.props.location.pathname);
    }

    render() {
        return (
            <Fragment>
                <PageHeader style={{borderColor: '#6dac8a'}}>
                    Music FM / Artists / Albums
                </PageHeader>
                {this.props.albums.map(album => (
                    <ListItem
                        key={album._id}
                        id={album._id}
                        title={album.name}
                        image={album.image}
                        link='/tracks/'
                        route={this.props.route}
                    >
                        <span style={{textAlign: 'center'}}> Год: {album.year}</span>
                    </ListItem>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAlbums: idArtist => dispatch(fetchAlbums(idArtist)),
        route: path => dispatch(push(path))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);