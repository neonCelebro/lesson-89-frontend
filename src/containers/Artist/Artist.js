import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {fetchArtists} from "../../store/actions/artists";

import ListItem from '../../components/UI/ListItem/ListItem';
import {push} from "react-router-redux";

class Artists extends Component {
  componentDidMount() {
    this.props.onFetchArtists();
  }

  render() {
    return (
      <Fragment>
        <PageHeader style={{borderColor: '#6dac8a'}}>
          Music FM / Artists
        </PageHeader>
        {this.props.artists.map(artist => (
                <ListItem
                    key={artist._id}
                    id={artist._id}
                    title={artist.name}
                    image={artist.image}
                    link='/albums/'
                    route={this.props.route}
                />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    artists: state.artists.artists
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists()),
      route: path => dispatch(push(path))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);