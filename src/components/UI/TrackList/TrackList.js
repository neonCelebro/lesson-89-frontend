import React , {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';

const styles = () => ({
    card: {
        display: 'flex',
        alignItems: 'center'
    },
    content: {
        flex: '1 0 auto',
    },
    playIcon: {
        height: 38,
        width: 38,
    },
});

class MediaControlCard extends Component{

    state = {
        isPlayed : false
    };

    butonHendlerControl =() => {
      this.setState({isPlayed: !this.state.isPlayed});
      !this.state.isPlayed && this.props.trackHendler();
    };

    render(){

        const { classes} = this.props;

        return (
            <div>
                            <Card className={classes.card}>
                                <CardContent>
                                    <Typography variant="headline">{this.props.name}</Typography>
                                    <Typography variant="subheading" color="textSecondary">
                                        {this.props.title}
                                    </Typography>
                                    <Typography variant="subheading" color="textSecondary">
                                        продолжительность : {this.props.time}
                                    </Typography>
                                </CardContent>
                                {
                                    this.state.isPlayed?
                                        <IconButton
                                            onClick={this.butonHendlerControl}
                                            aria-label="Pause">
                                <PauseIcon className={classes.playIcon}/>
                                        </IconButton>
                                        :
                                        <IconButton
                                            onClick={this.butonHendlerControl}
                                            aria-label="Play">
                                            <PlayArrowIcon className={classes.playIcon} />
                                        </IconButton>
                                }
                            </Card>
                </div>
        );

    }
}

MediaControlCard.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,

};

export default withStyles(styles, { withTheme: true })(MediaControlCard);