import React, {Component} from 'react';
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import Add from '@material-ui/icons/Add';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import {withStyles} from "@material-ui/core/styles/index";

const styles = {
    root: {
        flexGrow: 1,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class UserMenu extends Component{
    state = {
        user: null,
        add: null
    };


    handleUser = event => {
        this.setState({ user: event.currentTarget });
    };

    handleAdd = event => {
        this.setState({ add: event.currentTarget });
    };

    Route = (path) => {
        this.props.route(path);
        this.setState({ user: null, add: null });
    };

    handleClose = () => {
        this.setState({ user: null, add: null });
    };

    render(){

        const { classes } = this.props;
        const { user, add } = this.state;
        const openUser = Boolean(user);
        const openAdd = Boolean(add);

        return(
            <div>
                <IconButton
                    id={'menu-appbar'}
                    aria-owns={openUser ? 'menu-appbar' : null}
                    aria-haspopup="true"
                    onClick={this.handleUser}
                    color="inherit"
                >
                    <AccountCircle />
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={user}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={openUser}
                    onClose={this.handleClose}
                >
                    <MenuItem
                        onClick={()=> this.Route('track_history/')}
                    >
                        My account
                    </MenuItem>
                </Menu>
                <IconButton
                    id={'added'}
                    aria-owns={openAdd ? 'added' : null}
                    aria-haspopup="true"
                    onClick={this.handleAdd}
                >
                    <Add />
                </IconButton>
                <Menu
                    id="added"
                    anchorEl={add}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={openAdd}
                    onClose={this.handleClose}
                >
                    <MenuItem
                        onClick={()=> this.Route('/add_Artist')}
                    >
                        Add Artist
                    </MenuItem>
                    <MenuItem
                        onClick={()=> this.Route('/add_Album')}
                    >
                        Add Album
                    </MenuItem>
                    <MenuItem
                        onClick={()=> this.Route('/add_Track')}
                    >
                        Add Track
                    </MenuItem>
                </Menu>
            </div>
        )
    }
}

export default withStyles(styles)(UserMenu)