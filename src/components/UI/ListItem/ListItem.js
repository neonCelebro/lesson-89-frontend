import React , {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
    card: {
        width: 300,
        margin: "20px"
    },
    media: {
        height: 0,
        paddingTop: '56.25%',
    },
};

const url = 'http://localhost:8000/uploads/';



class ListItem extends Component{

    route = () => {
        this.props.route(this.props.link + this.props.id)
    };

    render(){

        const {
            classes,
            image,
            title,
            description,
            children
        } = this.props;

        return (
            <div style={{display: "inline-block"}}>
                <Card className={classes.card}>
                    <CardMedia
                        className={classes.media}
                        image={url + image}
                        title='album'
                    />
                    <CardContent>
                        <Typography gutterBottom variant="headline" component="h2">
                            {title}
                        </Typography>
                        <Typography component="p">
                            {description}
                        </Typography>
                        <Typography component="p">
                            {children}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button
                            variant="raised"
                            color="secondary"
                            onClick={this.route}
                            size="small"
                        >
                            Go!
                        </Button>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

ListItem.propTypes = {
    classes: PropTypes.object.isRequired,
    image: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    route: PropTypes.func.isRequired,
    link: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
};

export default withStyles(styles)(ListItem);
